function createCard(title, description, pictureUrl, starts, ends, location) {
    return `
    <div class="col-4 g2">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        <p class="card-text">${starts} - ${ends}</p>
        </div>
      </div>
    </div>
    `;
  }

function createError(error, message) {
    return `
        <div class="alert alert-danger" role="alert">
            ${error} - ${message}
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        const html = createError(response.status, response.statusText)
        const row = document.querySelector('.row');
        row.innerHTML += html
    } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts).toDateString();
            const ends = new Date(details.conference.ends).toDateString();
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, starts, ends, location);
            const row = document.querySelector('.row');
            row.innerHTML += html;
            console.log(html);
          }
        }

      }
    } catch (e) {
      const html = createError(e)
      const row = document.querySelector('.row');
      row.innerHTML += html
    }

  });
