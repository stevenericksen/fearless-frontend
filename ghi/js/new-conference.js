window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations';

    const response = await fetch(url);
    if (response.ok) {
        const responseData = await response.json();
        const locationsData = responseData.locations;
        const selectTag = document.getElementById('location');
        for (let location of locationsData) {
            const option = document.createElement('option');
            option.value = location.id;
            option.textContent = location.name;
            selectTag.appendChild(option);
            console.log(option)
        }
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceURL = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(conferenceURL, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    });
});
